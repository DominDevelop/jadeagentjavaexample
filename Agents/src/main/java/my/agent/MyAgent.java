package my.agent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import behaviours.ReceiveOneMessageBehaviour;
import behaviours.SendOneMessageBehavior;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

public class MyAgent extends Agent {
	
	private Integer lpAgent;
	
	private boolean sequential;

	private List<AID> listAgents;

	public MyAgent() {
		this.listAgents = new ArrayList<AID>();
	}

	@Override
	protected void setup() {
		super.setup();
		this.registerService(true, getAID());

		if (getArguments() != null) {
			Object[] args = getArguments();
			if(args.length > 0){
				String arg1 = args[0].toString();
				String arg2 = args[1].toString();
				
				lpAgent = Integer.valueOf(this.getLocalName().substring(this.getLocalName().length() - 1)); 

				if (arg1.equals("1")) { // (1,T) (1,F)
					this.registerService(false, getAID());
					if(arg2.equals("T")){
						this.sequential = true;
						//System.out.println(" Agent " + this.getLocalName() + " uruchomiony z TOKENEM w trybie sekwencyjnym");
						super.addBehaviour(new SendOneMessageBehavior(this, true));
					}
					if(arg2.equals("F")){
						this.sequential = false;
						//System.out.println(" Agent " + this.getLocalName() + " uruchomiony z TOKENEM w trybie losowym");
						super.addBehaviour(new SendOneMessageBehavior(this, false));
					}
				} else if (arg1.equals("0")) { //(0,T) (0,F)
					//System.out.println(" Agent " + this.getLocalName() + " uruchomiony bez TOKENU, agent rejestruje się");
					if(arg2.equals("T")){
						this.sequential = true;
					}
					if(arg2.equals("F")){
						this.sequential = false;
					}
				} 
			}
		}

		super.addBehaviour(createMassageReceiverUpdateBehaviour());
		super.addBehaviour(new ReceiveOneMessageBehaviour(this));

	}
	
	public boolean isSequential() {
		return sequential;
	}

	public void setSequential(boolean sequential) {
		this.sequential = sequential;
	}

	public int getLpAgent() {
		return lpAgent;
	}

	public void setLpAgent(int lpAgent) {
		this.lpAgent = lpAgent;
	}

	public void registerService(boolean register, AID aid) {

		ServiceDescription sd = new ServiceDescription();
		sd.setType("Message-Receiver");
		sd.setName(aid.getLocalName() + "-Message-Sender");

		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(aid);
		dfd.addServices(sd);

		try {
			if(register){
				//System.out.println(getLocalName() + " rejestruje się");
				DFService.register(this, dfd);
			}else{
				//System.out.println(getLocalName() + " wyrejestrowuje się");
				DFService.deregister(this);
			}
		} catch (FIPAException fe) {
			//fe.printStackTrace();
		}
	}

	public Behaviour createMassageReceiverUpdateBehaviour() {

		return new TickerBehaviour(this, 10) {
			protected void onTick() {
				ServiceDescription sd = new ServiceDescription();
				sd.setType("Message-Receiver");

				DFAgentDescription template = new DFAgentDescription();
				template.addServices(sd);

				try {
					DFAgentDescription[] result = DFService.search(myAgent, template);
					listAgents.clear();
					for (int i = 0; i < result.length; ++i) {
						listAgents.add(result[i].getName());
					}
				} catch (FIPAException fe) {
					fe.printStackTrace();
				}
			}
		};

	}

	public List<AID> getListAgents() {
		return listAgents;
	}

	public void setListAgents(List<AID> listAgents) {
		this.listAgents = listAgents;
	}
}
