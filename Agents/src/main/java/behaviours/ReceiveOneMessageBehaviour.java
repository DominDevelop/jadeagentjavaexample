package behaviours;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;
import my.agent.MyAgent;

public class ReceiveOneMessageBehaviour extends TickerBehaviour {

	public ReceiveOneMessageBehaviour(Agent agnet){
		super(agnet, 500);
		this.myAgent = agnet;
	}

	@Override
	protected void onTick() {
	
        ACLMessage message = myAgent.receive();
        
        if (message != null) {
            if (message.getPerformative() == ACLMessage.INFORM) {
            	
            	((MyAgent) this.myAgent).registerService(false,((MyAgent) this.myAgent).getAID() );
            	
                PackageMessage packageMessage = null;
                try {
                	packageMessage = (PackageMessage)message.getContentObject();
				} catch (UnreadableException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
  
                System.out.println(myAgent.getAID().getLocalName() + " odebrał wiadomość " + packageMessage.getMessage());
                myAgent.addBehaviour(new SendOneMessageBehavior(myAgent, ((MyAgent) this.myAgent).isSequential()));
                
            } else {
                System.out.println(message);
            }
        }
	}
	
}
