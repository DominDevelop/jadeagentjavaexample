package behaviours;

import java.io.Serializable;

public class PackageMessage implements Serializable {

	//-gui "Tomek1:my.agent.MyAgent(1,T) ;Kasia2:my.agent.MyAgent(1,T);Waldek3:my.agent.MyAgent(1,T);Ziomek4:my.agent.MyAgent(0,T)"
	
	
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
