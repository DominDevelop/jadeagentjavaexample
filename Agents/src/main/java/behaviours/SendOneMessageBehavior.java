package behaviours;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.WakerBehaviour;
import jade.lang.acl.ACLMessage;
import my.agent.MyAgent;

public class SendOneMessageBehavior extends WakerBehaviour {

	private MyAgent agent;
	
	private boolean sequential;
	
	public SendOneMessageBehavior(Agent agent, boolean sequential) {
		super(agent, 1500);
		this.agent = (MyAgent)agent;
		this.sequential = sequential;
	}

	@Override
	protected void onWake() {

		//System.out.println("wysyłanie wiadomości przez " + this.agent.getLocalName());
		List<AID> messageReceivers = agent.getListAgents();

		if (!messageReceivers.isEmpty()) {
			agent.registerService(false, this.agent.getAID());
			ACLMessage message = new ACLMessage(ACLMessage.INFORM);

			message.setSender(agent.getAID());
			PackageMessage packageMessage = new PackageMessage();
			packageMessage.setMessage("wiadomość od " + agent.getLocalName());
			try {
				message.setContentObject(packageMessage);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			if(sequential){
				message.addReceiver(getSequentialReceiver(messageReceivers, agent));
			}else{
				message.addReceiver(getRandomReceiver(messageReceivers, agent));
			}
			agent.send(message);
			agent.registerService(true, this.agent.getAID());
		}
	}

	private AID getRandomReceiver(List<AID> messageReceivers, MyAgent agent){
		
		int random = (int)(Math.random() * messageReceivers.size());
		return messageReceivers.get(random);
	}
	
	private AID getSequentialReceiver(List<AID> messageReceivers, MyAgent agent){

		int i = Integer.valueOf(agent.getLocalName().substring(agent.getLocalName().length() - 1)); 
		
		AID first = messageReceivers.get(0);
		int min = Integer.valueOf(messageReceivers.get(0).getLocalName().substring(messageReceivers.get(0).getLocalName().length() - 1)); 
		for(AID aid : messageReceivers){
			
			int var = Integer.valueOf(aid.getLocalName().substring(aid.getLocalName().length() - 1)); 
			if(min > var){
				min = var;
				first = aid;
			}
			
			if(var == i+1){
				return aid;
			}
		}
		return first;

	}
}
